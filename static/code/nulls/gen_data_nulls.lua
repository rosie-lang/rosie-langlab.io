-- -*- Mode: Lua; -*-                                                               
--
-- gen_data_nulls.lua
--
-- © Copyright Jamie A. Jennings 2019.
-- LICENSE: MIT License (https://opensource.org/licenses/mit-license.html)
-- AUTHOR: Jamie A. Jennings

assert(math)
assert(string)

function random_non_null()
   return math.random(1, 255)
end

function write_non_nulls(f, n)
   for i = 1, n do
      f:write(string.char(random_non_null()))
   end
end

function make_fibonacci()
   local n, m = 0, 1
   return function()
	     local sum = n + m
	     n = m
	     m = sum
	     return sum
	  end
end

function write_chars(f, n)
   local i = 0
   local fib = make_fibonacci()
   local next_null = fib()
   while i < n do
      write_non_nulls(f, next_null - i - 1)
      i = next_null - 1
      if i >= n then break; end
      f:write(string.char(0))
      i = i + 1
      next_null = fib()
   end
end


--     > f = io.open("/tmp/hasnulls.txt", "w")
--     > write_chars(f, 144)
--     > f:close()
--     true
--     >
