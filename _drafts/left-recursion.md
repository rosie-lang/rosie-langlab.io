
## Direct Left Recursion

Examples are from "Packrat Parsers Can Support Left Recursion" by Alessandro
Warth, James R. Douglass, Todd Millstein.  (To be published as part of ACM
SIGPLAN 2008 Workshop on Partial Evaluation and Program Manipulation (PEPM ’08)
January 2008.)

``` 
term ::= <term> "+" <fact>
       / <term> "-" <fact>
       / <fact>
fact ::= <fact> "*" <num>
       / <fact> "/" <num>
       / <num>
``` 


First we note that this example is of direct left recursion, and we want a
solution that will work for indirect as well.  But let’s start here, with this
example.

Since RPL is compiled, we should ask, “What code would we generate for term and
fact, if we handled left recursion?”  Let’s look at fact first.

	Main program:
		Call term
		End

	term:	...

	fact:
		Call num		// fail if num fails
		Push choice #div
		Char “*” 		// fail will pop stack and jump to #div
		Call num		// fail will pop stack and jump to #div
		Return

	#div:
		Push choice #epsilon
		Char “/“ 		// fail will pop stack and jump to #epsilon
		Call num		// fail will pop stack and jump to #epsilon
		Return

	#epsilon:
		Return

The code for fact looked deceptively easy.  What happens when we move closer to
RPL from this pseudo-code by inserting capture instructions?  We must ensure
that we get the structure we want as a final return value.

	fact:
		Open capture “fact” 
		Open capture “fact” 
		Call num		// fail if num fails
		Close capture   // fact->num
		Push choice #div
		Char “*” 		// fail will pop stack and jump to #div
		Call num		// fail will pop stack and jump to #div
		Close capture   // fact->(fact->num "*" num)
		Return
	#div:
		Push choice #epsilon
		Char “/“ 		// fail will pop stack and jump to #epsilon
		Call num		// fail will pop stack and jump to #epsilon
		Close capture   // fact->(fact->num "/" num)
		Return
	#epsilon:
		Close capture   // fact->fact->num
		Return

N.B. If we decide we do not want/need `fact->num`, then we can simplify the code
above by removing an `Open capture "fact"` and its `Close capture`.

This looks like it should work.  I’ll assume for the moment that it will
generalize to the case where there are multiple rules that are not left
recursive, i.e. not just num.  (I am imagining a partitioning of the rules into
left recursive and non, where each set preserves the relative order of the
rules.  This partitioning would appear to violate the PEG ordered choice,
though, so it bears more examination.)

Ok so two directions to go from here:

1. Explore the case noted above in which there are multiple left recursive rules
and multiple non left recursive rules, and they are interleaved in some perverse
way.

2. Start with the straightforward example we just saw and examine how it
generalizes to indirect left recursion (by changing the example slightly).


## Indirect Left Recursion

``` 
   x    ::= <expr>
   expr ::= <x> "-" <num> 
          / <num>
   
``` 

How would we generate code for this grammar in RPL, if we supported left
recursion?

In the current RPL 1.1, the statement `x = expr` will create a copy of `expr`
whose outermost capture is `x` instead of `expr`.  What the grammar above is
intended to mean, though, is that a match using `x` will have an `x` node at the
root of the resulting parse tree.  This would be written in RPL as `x =
{expr}`, and we will assume this is what the user wants.  In other words, using
the above grammar, a match of pattern `x` against input "1" would yield

	x -> expr -> x -> expr -> num
	
We could argue that this is not what the output should look like, but this is
how the grammar was written, so presumably it's what the user wants.

	Main:
		Call x
		End

	x:
		Open capture "x"
		Call expr         // x fails if expr fails
		Close capture     // x->expr->...
		Return

	expr:
		// We don't call x due to the resulting left recursion,
		// So we try the other rule for expr first.
		Open capture "expr"
		Open capture "x"
		Open capture "expr"
		Call num	      // fail if num fails
		Close capture     // expr->num
		Close capture     // x->expr->num
		Push choice #epsilon
		Char "-"          // fail will pop stack and jump to #epsilon
		Call num          // fail will pop stack and jump to #epsilon
	#epsilon:
		Close capture     // expr->(x->expr->num "-" num)
		Return            //   OR expr->x->expr->num

This looks like it should work.  (And soon we will be able to hand-code it to
try it out.)  Meanwhile, we also note that `expr` remains callable.  That is,
the code generated for `expr` is correct when `expr` is the entry point instead
of `x`.

Will a second level of indirection change anything?  What about a more
complicated definition of `x`?



