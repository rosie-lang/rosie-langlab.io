---
layout: post
title:  "Rosie v0.99 available now!"
date:   2017-06-14
categories: release
tags: first-post
---

The current version is [Rosie
v0.99k](http://gitlab.com/rosie-pattern-language).
Rosie is more powerful than regex, easier to use, and faster!

Follow us on [Twitter](https://twitter.com/jamietheriveter) for
announcements.  We expect v1.0.0 to be released later this summer.

