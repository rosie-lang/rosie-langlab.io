---
layout: post
title:  "Getting started (tutorial)"
date:   2019-07-11
permalink: /start/
categories: 
---

Welcome!

## Ways to use Rosie/RPL

There are several ways to use Rosie/RPL.  For many people, their goal is to use
the Rosie library in programs, instead of a regular expression library.  If you
are just getting started, though, it's faster to try out Rosie at the command
line in a terminal.

Whether you'll use Rosie at the command line or in a program, you'll have to
install Rosie itself.  Then, to program with Rosie, you'll install a language
module for the programming language you use.

### Installing

See the
[installation instructions](https://gitlab.com/rosie-pattern-language/rosie#installing),
particularly the section on *Local installation*.  

On a Mac, you can use `brew install rosie`, which requires a
[one-time setup step](https://gitlab.com/rosie-community/packages/homebrew-rosie#to-install-rosie-pattern-language-on-os-x)
to add the Rosie installation instructions to `brew`.  This installs Rosie so
that any user can run it at the command line with the `rosie` command.

<style type="text/css">
    <!--
      .sidebar {
	   font-family:sans-serif;
       background-color: #f0f0f0;
      }
    -->
</style>
<blockquote>
<div class="sidebar"> On Linux or other Unixes (or on MacOS, if you don't use <code>brew</code>):
<ol>
<li> <code>git clone https://gitlab.com/rosie-pattern-language/rosie.git</code></li>
<li> <code>cd rosie</code></li>
<li> <code>make</code></li>
</ol>

All Rosie files are within the Rosie directory that was created when the repo
was cloned.  The executable is under <code>bin/rosie</code>.  To install into
<code>/usr/local</code>, run <code>make install</code>.  

</div> </blockquote>


### Verifying that Rosie is installed

In the Rosie directory, run `bin/rosie version`.  Or, if you used `brew` or ran
`make install`, just type `rosie` at any command prompt.

The output should look something like this:

<center>
<img width="50%" src="{{ site.baseurl }}/images/rosie-version-command.png" 
alt= "At the command prompt, the user types 'bin/rosie' and the rosie program
responds with '1.1.0'">
</center>

If you get an error instead, make sure that the `brew` installation succeeded,
or that `make` succeeded if you did not use `brew`.

### In programs

The `brew` method compiles and installs Rosie.  You may now install a module for
your favorite programming language to use `librosie`, a library containing all
of the Rosie functionality.

<blockquote>
<div class="sidebar"> On Linux or other Unixes (or on MacOS, if you don't use <code>brew</code>):
<p>
If you did not use <code>brew</code>: The <code>make</code> process compiles Rosie but does not
install it.  The executable is available in the Rosie directory as <code>bin/rosie</code>.
The library <code>librosie</code> is built as well, and resides in <code>src/librosie/binaries</code>.
(There are multiple forms of the library, including static and dynamic/shared.)
</p><p>
Before you can use Rosie in a program, you should run <code>make install</code>, which will
install Rosie (including <code>librosie</code>) in the default system location
<code>/usr/local</code>.
</p>
</div></blockquote>


The last step is to install a language module.  If you are programming in
Python, you can use `pip install rosie`.  More information on the Python module
and interfaces for other languages can be found in the
[Rosie Community / Clients](https://gitlab.com/rosie-community/clients) repositories.

A separate *Getting Started* tutorial for each supported language is being
developed.  The rest of this tutorial will use the command line for exploring
the functionality of Rosie and the Rosie Pattern Language


## If you know regular expressions

If you are not very familiar with regular expressions (regex), you can skip to
the next section.

There is an [overview of RPL for people who know regex](doc/i-know-regex.md)
available in the Rosie documentation.  The highlights are:

- To find a literal string, put it in double quotes: `"nameserver"`
- RPL expressions contain whitespace between elements: `"nameserver" net.ip`
- By default, RPL assumes a token boundary (like whitespace) between elements,
  so there must be whitespace in the input text between `"nameserver"` and
  `net.ip`
- To disable tokenizing, put an expression in curly braces: `{"nameserver" " "
  net.ip}` matches `"nameserver"` then exactly one space, then `net.ip`
- The `rosie match` command starts with the first character of input; to search
  for a pattern, use the `rosie grep` command
- RPL expressions are possessive: `.* "x"` will always fail because `.*` will
  consume all available input

Also, the Rosie implementation has a variety of *output encoders*, which take
the output of a match and render it for you.  By default, the `rosie match`
produces colorized text, and the `rosie grep` command prints all lines
containing a match (like Unix `grep` does).

To see the detailed match data, use `-o jsonpp` after `rosie match` or `rosie
grep`.  This will pretty-print match data in JSON.


## RPL from scratch

