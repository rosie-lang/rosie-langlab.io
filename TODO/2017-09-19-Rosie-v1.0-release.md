---
layout: post
title:  "Rosie v1.0.0-alpha released!"
date:   2017-09-19
categories: announcement
---


## Rosie version 1.0.0-alpha released!

Today we released an alpha version of Rosie 1.0.0 (a few days ahead of the
autumnal equinox).  It has all the new features described in the recent
"Preview" posts.

We had to release 1.0.0-alpha without the API, unfortunately.  This is something
we will remedy in a matter of weeks.  Meanwhile, plenty can be done with Rosie's
CLI and REPL.  Please try them out, and give us feedback!

Open issues to report bugs or to request enhancements.  General feedback and
discussion can be posted on Reddit.  We will identify an appropriate subreddit
and post it on this blog shortly.

<hr>

Follow us on [Twitter](https://twitter.com/jamietheriveter) for
announcements.

