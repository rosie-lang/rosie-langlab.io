---
title: "Feedback"
description: "Suggest edits or open an issue"
lastmod: "01 Jan 2023"
---

<h3> Give us a star on GitLab </h3>

Let us know you found something worthwhile about this project by giving us a
star on [GitLab](https://gitlab.com/rosie-pattern-language/rosie), where the
code is hosted. 

<h3> Report a bug </h3>

If you want to report a bug or request a feature, please open an issue (or
submit a pull request) on GitLab.  We ask that you first search for a similar
issue on the [issue page for
Rosie](https://gitlab.com/rosie-pattern-language/rosie/-/issues).


<h3> Suggest an edit to this site</h3>

On each page of this site, there is a link in the footer on the right hand side
labeled "Edit on GitLab".  That link takes you to the markup source for the
page.

A pull request is the preferred way of proposing an edit, though if you find an
issue with _this site_ and would like to discuss it, please [open an
issue here](https://gitlab.com/rosie-lang/rosie-lang.gitlab.io/-/issues).





