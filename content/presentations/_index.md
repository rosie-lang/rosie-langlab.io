---
title: Presentations
weight: 100
---

*  [2019 Midwest PL Summit, Purdue University](/pres/RosiePatternLanguage-MWPLS-2019.pdf)
*  [2019 Detroit Tech Watch, Detroit MI](/pres/DetroitTechWatch2019.pdf)
*  [2019 Lambda Squared, Knoxville TX](/pres/LambdaSquared2019.pdf)
*  [2018 Strange Loop, St. Louis, MO](/pres/StrangeLoop2019.pdf)
   [Link to video (YouTube)](https://www.youtube.com/watch?v=MkTiYDrb0zg&list=PLcGKfGEEONaBUdko326yL6ags8C_SYgqH&index=11&t=0s)
   
   
---

In some of my talks, I use this
[short video](/pres/Python%20ip%20addr%20and%20uri%20regex.mp4) to illustrate why we
should not use sites like Stack Overflow as repositories for regex, just as we
(hopefully) would not do this for code.  The author of a gist or an answer on a
Q-and-A website makes no commitment to maintenance (nor should they).  And I
don't mean to pick on the author of the gist I found in this video -- all the
ones I found have reported bugs, in addition to the usual readability issues of
regex.


	

