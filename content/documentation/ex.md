---
layout: default
title: Examples
permalink: /ex/
---

The [language reference](https://gitlab.com/rosie-pattern-language/rosie/blob/master/doc/rpl.md)
contains some representative snippets that demonstrate Rosie's unique features,
and you might also like to look through the
[source code for Rosie's standard library modules](https://gitlab.com/rosie-pattern-language/rosie/tree/master/rpl)
such as the [date-parsing module](https://gitlab.com/rosie-pattern-language/rosie/blob/master/rpl/date.rpl).

Examples of Rosie v0.99, as well as some blog posts and Tech Talks, may be found on 
[IBM developerWorks Open](https://developer.ibm.com/code/search/?q=rosie).

There were some changes when Rosie v1.0.0 was released, so please be aware the
the IBM content may not match exactly what Rosie looks like today.
