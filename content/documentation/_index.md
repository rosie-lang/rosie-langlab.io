---
title: Documentation
weight: 30
---

* [Overview on gitlab](https://gitlab.com/rosie-pattern-language/rosie#rosie-pattern-language-rpl)

* [Documentation index](https://gitlab.com/rosie-pattern-language/rosie/blob/master/doc/README.md)

<!-- * [Rosie man page](/static/rosie.1.html) -->




