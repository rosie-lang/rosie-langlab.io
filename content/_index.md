---
title: "Home"
description: "Home page and latest news"
date: "1 Aug 2023"
---

<!-- <h1>About this book</h1> -->

<h3>What is the Rosie project?</h3>

In brief: RPL is an alternative to regex, providing a better syntax, unit tests,
and packages of patterns, among other benefits.

The Rosie project began at IBM within a large machine learning project that
required feature extraction from hundreds of text-based data sources.  We had
too many regular expressions to manage, and found them difficult to test,
maintain, and share.  

Sharing was particularly important because we wanted to harvest regexes already
in use within products whose data we were mining.  But regexes are not very
portable across languages.  Nor do they compose well, making it challenging if
not impossible to reliably build larger expressions out of smaller ones.

I left IBM in 2018 and brought the project to [North Carolina State
University](https://www.csc.ncsu.edu), where it has thrived with contributions
from undergraduate and graduate students.

On the left side navigation menu, you'll find blog posts about Rosie.  The
documentation menu is a placeholder, while we migrate documentation from GitLab
(where the code is hosted) to this site, where it can be better organized.


<h3>News</h3>

* 2023-07-15.  Rosie 1.4.0 was released.  In a new blog post, I write about what
  is coming in "Rosie 2.0".




