---
draft: true
title:  "To Parse or Not to Parse?"
description: "DRAFT"
lastmod: "2019-06-10"
---

Is Rosie a parser or a pattern matcher, and what is the difference?  In this
post, I take a practical approach to these questions.  Along the way, I present
my thoughts on when to parse and when not to parse.

<!-- ----------------------------------------------------------------------------- -->
## This is a long essay

Playlist: 
* The long and winding road
* Take the long way home

Contents:


<!-- ----------------------------------------------------------------------------- -->
## Background and history

### Text search

A [common definition](https://en.wikipedia.org/wiki/Regular_expression) of
*regular expression* uses the phrase "search pattern".  Ken Thompson's 1968 CACM
article (Volume 11, Number 6, *Regular Expression Search Algorithm*) uses the
terms "search", "match", and "found", but avoids "pattern" in favor of the
phrase "regular expression".  I suspect that Thompson wanted to emphasize the
innovation of using regular expressions for text search, implicitly contrasting
the approach to less powerful search techniques that came before.  Presumably,
some of the existing text search algorithms used idiosyncratic "expressions" or
"patterns" of their own (as opposed to allowing only the search for literal
substrings).

In formal language theory, we speak of *recognizers* and *generators* of a
language.  Here, a language is a set of strings described in a compact way, such
as by a regular expression or some flavor of grammar.  Those descriptions let us
specify an infinite language using a finite set of symbols; for example, the
regular expression `a*b` describes the set of strings consisting of zero or more
`a` characters followed by a `b`.

A *recognizer* of a language is, essentially, an algorithm for deciding whether
a given input string is part of the language.  A recognizer for `a*b` would say
**yes** to the input "aaab" and **no** to the input "aba".  Here we are less
concerned with *generators* of the strings in a language, although that is a
fascinating topic.  (Search for "computational narrative" generally, or
see, Kate Compton's [Tracery](http://tracery.io) for a wonderful usable tool
with many hilarious and thought-provoking examples.)

Thompson's 1968 innovation was to use a particular formal language (Kleene's
*regular languages*) as the basis for a text searching function.  When I look at
text search as a design problem, I see three strong pillars of support for
Thompson's solution here:

1. Regular languages are powerful, giving the user a single technique that can
   be used in a wide array of text searches.
2. Regular languages have a really compact notation, namely *regular
   expressions*. At a time when teletypes and line editors were common, and
   communication speeds very low, a concise notation wins the day.
3. Regular expressions can be converted into recognizers efficiently, and such
   recognizers can execute (process input text, looking for matches)
   efficiently. <a name="cite-note-1">&lbrack; [Note 1](#note-1) &rbrack;</a>

Needless to say, regular expressions caught on.  To suit new use cases, though,
they have evolved considerably.  New technologies based on regular expressions
remain in wide use today, more than 50 years later.

What we call *regex* today are technically not regular expressions at all.
Kleene's regular expressions are barely visible beneath accreted layers that
provide captures, back references, bounded repetitions, Unicode support, and
control over the matching algorithm itself <a name="cite-note-2">&lbrack;
[Note 2](#note-2) &rbrack;</a>.  Some variants even support "recursive regular
expressions", though that is an oxymoron.  Perhaps we should simply call the
expressions *patterns*.

Some patterns happen to be (Kleene) regular expressions, while others use
captures and back references, and still others are recursive.

Regular expressions, and now *regex*, are patterns used to search the world's
text, noting the presence (and absence) of strings that are important to the
user.  What, then, is *parsing*?

### Parsing

Returning to formal language theory, we find that there is a hierarchical
relationship among some sets of languages, i.e. the
[Chomsky hierarchy](https://en.wikipedia.org/wiki/Chomsky_hierarchy).  Here,
regular languages are the smallest set.  Context-free languages are a larger set,
including all the regular languages and some non-regular ones.  The set of
context-sensitive languages is, in turn, larger than the set of context-free
languages, and so on.

What does this mean for text searching (for which we will now use the generic
term *pattern matching*)?

Regular languages describe simple patterns in text.  Context-free languages can
describe all of those simple patterns, plus certain more complex ones.  (And so
on.)

I think it is largely for historical/cultural (not technical) reasons that we
say *matching* when describing what a regular language recognizer does, but we
say *parsing* when describing what a context-free language recognizer does.  (We
also speak of context-sensitive language *parsing*.)

Because regular expressions became widely known to computer scientists as a tool
for text search, we say that a regular expression *matches* the input text (or a
substring of it).  That is, we use the terminology of searching for and finding
matches.  Meanwhile, linguists were modeling the *parsing* of natural language
using larger language sets from the Chomsky hierarchy, such as context-sensitive
grammars.  Here, the language descriptions are *grammars*, and a recognizer for
a grammar is a *parser*.

Is it, then, just an accident of history that the term *matching* is used with
regular expressions and *parsing* with context-free and other grammars?  I
believe that it is. However, the last 50 years have seen the development of:

* a variety of algorithms for regular expression matching, context-free
  parsing, and context-sensitive parsing; as well as

* a multitude of other formal languages, like the many *regex* flavors and
  Parsing Expression Grammars (more on these later), and recognition algorithms
  for each.

With so many flowers blooming, the scientist starts to classify them by their
attributes, in order to gain some understanding of the overall landscape.  And
what do we find?  Textual *pattern languages* (the *regex* flavors) share a number
of technical attributes with each other, but not with the *parsed languages* of
context-free and other grammars.  And vice-versa.

Next, we examine how matchers and parsers differ.

<!-- ----------------------------------------------------------------------------- -->
## Distinctions and differences

First, let's acknowledge the common ground between matchers and parsers that we
find today in code libraries and occasionally as integral features of a
programming language.  They take two arguments, a "language description" (e.g. a
regular expression or a grammar) and some input text, and produce a data
structure containing evidence of recognizing the described language in the input
text. 

Again, I mean *recognizing* as a generic term for *matching* or *parsing*.  But
why do we need the notion of evidence?  Formally, a recognizer need only return
a boolean indicating whether the input is in the described language, for example
**yes**, the input string "b" is in the language `a*b`.  

Most, if not all, implementations of recognizers do more than this.  They build
a data structure containing evidence in the form of *match(es)* or *parse(s)*
that prove the input string is in the described language.  (The converse is
typically not true: most implemented recognizers return **null** or **false** in
lieu of a proof that the input string is not in the described language.  <a
name="cite-note-3">&lbrack; [Note 3](#note-3) &rbrack;</a>)

The figure below divides implementations into two overlapping categories,
Pattern Matching and Parsing.  It may an imprecise taxonomy.  Nonetheless, we
will examine the rationale for it in the sections that follow.  Specifically, we
will look at the language descriptions themselves, the data structures produced
as output, and the use cases for each category.

<img width="100%" 
	 src="{{ site.baseurl }}/images/PatternMatchingOrParsing.png" 
	 alt="A Venn diagram with two overlapping regions. The region on the left is
	 labeled Pattern Matching. Points in this region are labeled
	 &quot;glob&quot;, regular expressions, regex, and lexer. The region on the
	 right is labeled Parsing and contains points labeled Perl6 Grammars, ANTLR,
	 Marpa, Bison, and Parsec.  The intersection of the two regions contains two
	 points, one labeled PCRE and one labeled PEG (RPL)."/>

<a name="figure-1"><p align="center">Figure 1: Pattern Matchers and Parsers</p></a>

### Language descriptions

To use a pattern matcher or a parser, the developer supplies as an argument a
*language description* that specifies which input strings match and which are
rejected. 

#### Pattern matchers

Let's look at the examples in the Pattern Matching region of the figure above,
to see how they describe languages (sets of strings).

* The "glob" expressions are simple ones in which `*` stands for any sequence of
  characters and `?` stands for any single character.  These originated in early
  Unix shells, where they let the user specify sets of files, e.g. `*.txt`.
* Regular expressions (from formal language theory) have more complex structure
  than "glob" expressions.  In fact, regular expressions are defined recursively
  as: the empty string, literal characters (like `a`, `b`), 0 or more
  repetitions of regular expressions (using the postfix `*` operator), sequences
  of regular expressions (like `a*b`), and choices between regular expressions
  (like `a|b*|c`).  Parentheses are used for grouping.
* The Posix Basic Regular Expressions start with the formal definition, but
  notably omit the choice operator (`|`), and then add such practical additions
  as:
    * the dot `.`, which matches any single character;
	* caret `^` and dollar `$`, which match at the start and end of the input
      string, respectively;
	* bounded repetitions using a postfix `{n,m}` syntax;
	* bracket expressions which denote sets of characters, any of which may
      match;
    * sub-expressions, denoted by parentheses; and
	* back-references to sub-expressions, denoted by number.
* The Posix Extended Regular Expressions consist of the Basic Regular
  Expressions plus:
    * the choice operator, `|`, to specify alternatives;
    * the postfix `+` operator which matches 1 or more repetitions; and
	* the postfix `?` operator which matches 0 or 1 repetitions.
* The last Pattern Matching example is the generic *lexer*.  A lexer (also
  called a *scanner*) is a tool for partitioning a character sequence into
  multi-character groups called tokens.  The user of a lexer defines each token
  type using a regular expression.  For example, a token definition for a word
  might be `[a-zA-Z]`, i.e. the set of upper and lower case English letters. For
  more, see,
  e.g. [Lexical Analysis](https://en.wikipedia.org/wiki/Lexical_analysis). 

<blockquote style="background-color: #e9e9e9">

**Note:** I have omitted many details about Posix Regular Expressions; for more,
see, e.g. 
[the Posix 2017 standard](http://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap09.html).
Also, Lexers/Scanners have a rich history and many implementations of their own.
For more on those, see, e.g. 
[the Dragon Book](https://en.wikipedia.org/wiki/Compilers:_Principles,_Techniques,_and_Tools).

</blockquote>

Examining the syntaxes of the Pattern Matchers listed above, we see that
expressions are a common theme.  Until we considered lexers, every language
description was an expression consisting of literal characters and characters
with special meaning, like `.` or `$`.  With lexers, we implicitly introduced
the notion of *binding* a symbol to an expression.  To define token types, we
give them names like IDENTIFIER, INT, FLOAT, LBRACK, etc.  Each name is a
symbol that we associate with (bind to) a regular expression.  

Crucially, though, the set of named regular expressions that we input to a lexer
is just that and no more: associated with each name is a regular expression, not
some combination of other names.  You might imagine a more sophisticated kind of
"token" whose definition would combine other named tokens, such as:

    CONDITIONAL = IF LPAREN EXP RPAREN THEN STATEMENT

The use of names like `EXP` and `STATEMENT` make the definition above a *grammar
rule* (also called a *production*), and take us out of the realm of lexical
analysis.  The concept of a lexical token does not allow such sophistication.
Tokens are defined by simpler patterns, traditionally some flavor of regular
expressions. 

#### Parsers

By contrast, the examples in the Parser region of our Venn diagram are designed
to allow complex definitions of structure such as the `CONDITIONAL` example that
ended the last section.  Because they vary so much one to the next, I won't
describe the Parser examples individually.  Instead, let's focus on the fact
that the user of any of these tools uses a grammar to describe the language that
the parser will recognize in the input text.

Grammars can describe more intricate structure than can regular expressions,
including recursive structure.  To continue our previous example, we might find
both of these rules in a single grammar:

    CONDITIONAL = IF LPAREN EXP RPAREN THEN STATEMENT
	STATEMENT = ASSIGNMENT | PROCEDURE_CALL | CONDITIONAL
	
In this simple example, a `CONDITIONAL` is a kind of `STATEMENT`, and it also
contains a `STATEMENT` (which may be another `CONDITIONAL`).

The grammars that we write as parser users are (almost always) context-free
grammars (CFG).  Sometimes, a CFG is ambiguous, meaning that a single input
string can be parsed multiple ways.  One parser may return all possible ways of
parsing the input, while another parser may return just one.  The parsers
developed for "artificial languages" (like programming languages and data
formats) provide ingenious ways of helping the user specify exactly which parse
is the desired one, such as using operator precedence and associativity.

Meanwhile, linguists trying to parse natural (human) language have concluded
that context-free grammars are insufficient, i.e. too simple.  They have,
broadly speaking, focused on context-sensitive grammars and similar formal
models.

Both communities, computer scientists and linguists, have created sophisticated
tools for parsing.  While the problems and the tools differ, both are centered
around grammars as language descriptions.

While the categories of Pattern Matching and Parsing may be imprecise, we can
see a bright contrast across the examples we have considered.  The pattern
matchers employ only regular expressions or regex variants to define the
languages they accept, while the parsers employ grammars that can describe more
intricate structure.

### Data structures

Next, we examine the data structures produced by Pattern Matchers and Parsers.
Here, the differences are less profound than was the case for language
descriptions. 

#### Pattern matchers

The simplest recognizer produces only a boolean, indicated whether a match was
found.  When using a pattern matcher, this may suffice.  For example, input
validation is often done using regex.  If some user input matches a certain
regex, the input is considered valid and the program continues.  (We pause to
note that many security vulnerabilities are due to incorrect input validation,
which raises the question of whether regular expressions are the right
technology for this task.)

When searching text for a substring that matches a pattern, a boolean usually
does not suffice.  We want the recognizer to produce the start and end indices
of the input text that match the pattern.  These indices are an ordered pair
that specify a "slice" of the input text; this is more information than a
boolean.  And what if we want to find all matches of the pattern?  Then the
output might be a list of ordered pairs or slices.  

Next, we consider patterns in which sub-expressions have been marked in some
way, such as by parentheses in regex which denote "captures".  If we have marked
some sub-expressions of interest, then presumably we want to know where they
start and end in the input text.  What if the sub-expressions themselves have
sub-expressions?  For example:

    (a*)(b+(c+)d)

The matching algorithm in this case might return to us a list of ordered pairs,
and in that list we would find the coordinates of the overall expression
match(es) and each of the sub-expressions -- but how would we know which is
which?  Which ordered pair is the possibly-empty sequence of `a`s, for instance? 

A list of ordered pairs appears to fall short here.  There are structural
relationships among the sub-expressions, and they form a tree with the overall
expression at the root.  In the example above, the root has two children, `(a*)`
and `(b+(c+)d)`.  The latter contains a sub-expression, so its tree node has a
child for `(c+)`.

<img width="100%" 
	 src="{{ site.baseurl }}/images/Tree-regex-captures.png" 
	 alt="A tree diagram where the root is labeled '(a\*)(b+(c+)d)'.  The root
	 has a left child labeled '(a\*)' and a right child labeled '(b+(c+)d)'.
	 The right child has one child, labeled '(c+)'."/>

If the matching algorithm returned a tree data structure in this format, with an
ordered pair attached to each node, we would have all of the available
information about the matches of the overall expression and all marked
sub-expressions.

However... The structure of such a tree is fixed by the form of the regular
expression.  We could choose a way to number the nodes of the tree above, say in
prefix order.  The root is 0, its left child is 1, and its right child is 2.
The child of the right child, `(c+)`, is 3.  We could design our recognizer to
output, for the input expression `(a*)(b+(c+)d)`, exactly 4 ordered pairs in the
specified order.  In fact, this is how regex captures are numbered.

So while a tree data structure is illuminating, a list suffices.

In considering only pattern matchers, we have seen output data structures
ranging from a single boolean to a specially ordered list of ordered pairs.
What about parsers?

#### Parsers

Parsers rarely produce any simpler output than a tree, for the simple reason
that the complete structure of a parse tree is not known in advance.
Consequently, we need a tree data structure if we want to examine how the input
text was parsed by the given grammar.

Continuing our simple programming language grammar example, a parser might
produce this tree as output:

<img width="100%" 
	 src="{{ site.baseurl }}/images/CONDITIONAL-tree.png" 
	 alt="A tree diagram where the root is labeled 'STATEMENT'.  The root
	 has one child labeled 'CONDITIONAL', which has two children. The left is
	 labeled 'EXP' and the right 'STATEMENT'. The right child has one child of
	 its own, labeled 'ASSIGNMENT'.  Lines emerge from the bottom of this node
	 to suggest that it has children, but none are shown."/>

The tree's root is a `STATEMENT`, apparently a conditional statement.  Inside
the conditional is another `STATEMENT`, this one an assignment.  We omitted the
details of how assignments are structured.

Also omitted from the tree diagram are the ordered pairs corresponding to the
slice of the input that matched each node.  In fact, most parsers require as an
argument not a text string, but a string (sequence) of tokens.  So, in most
cases, a tree like the one diagrammed above would have nodes containing token
information: what type of token (its name) and where was it found (an ordered
pair, or slice).

In compiler textbooks, lexing and parsing are treated separately, with lexing
defined as a transformation from a character sequence to a token sequence, and
parsing a transformation from token sequences to parse trees.  The distinction
here is one of convention, not of necessity.  The grammars used in parsing are
strictly more powerful than regular expressions, meaning they can do everything
that regular expressions can do, plus more.  Consequently, we can use grammars
to define tokens.
([Scannerless parsers](https://en.wikipedia.org/wiki/Scannerless_parsing) make
it easy to do this, and we will return to this topic later.)

<!-- If we wish, we can restrict our token definitions to the -->
<!-- regular languages simply by ensuring that each token definition contains no -->
<!-- references to other token names or grammar rules. -->

Whether we think about a parser operating on the output of a lexer or directly
on the input text, the most useful output is a tree because its structure is
largely unpredictable.  Maybe our grammar is written such that a parser will
produce a `STATEMENT` or a parse failure -- in this case the root of the output
tree is predictable.  But will the first parsed statement be an assignment?  Or
a conditional as shown in the tree diagram above?  And in a given conditional,
what kind of `STATEMENT` is there?

The trick of numbering the nodes in a fixed-structure tree does not work here.

Finally, we note that some grammars are ambiguous (like this
[famous example](https://en.wikipedia.org/wiki/Dangling_else)).  Therefore, for
some input strings, there exist multiple valid parse trees.  If we want all
possible information in such a case, then our parser should produce a forest
consisting of one or more trees in the case of a valid parse.

The distinction, then, between the output of a typical pattern matcher and a
typical parser is one between a list (or boolean) and a tree.


### Use cases

The last difference we will consider lies in the use cases for Pattern Matchers
and Parsers.  As we have noted, the history of textual pattern matching is in
text search.  Beyond search, other common uses include input validation
(producing a boolean result) and lexical analysis (producing a sequence of
tokens).  Let's look at the use cases for parsers.

#### Input processing

A very common use case for Pattern Matching is processing textual input provided
to an application by a user, either by direct entry or indirectly through a file
or database field which itself was written by a user.  (Or even more indirectly,
the input to our application could be the output of another application.)

Input could be an email address entered into a web form.  It could be a "white
list" of URLs read from a file.  More structured input might be written in JSON
or YAML format.  Data formats like JSON and YAML were designed to be readable
and writable by both programs and people (unlike, say, XML), making them a
common choice for configuration files.

Simple forms of input are typically processed by regex, or by hand-written tools
with similar capabilities.  On the other hand, structured formats like JSON,
YAML, and XML are defined by (perhaps imperfect) standards.  Each such standard
specifies all the details of the format, including a fixed grammar.  For
example, the rules that govern [what is valid JSON](https://json.org) include a
grammar that can be used by a developer to write a universal JSON parser.  When
the grammar for a format is fixed, we don't need regex, nor a lexer, nor a
parsing tool.  We can all use a universal JSON processor provided as a library
function. 

<!-- Natural language processing is a use case for parsers that we will not address -->
<!-- here.  Artificial languages are our focus, and these are primarily data -->
<!-- descriptions (e.g. JSON, XML, YAML) and programming languages.   -->

What if the input has a complicated structure that does not conform to a
standard like YAML or JSON?  What if the input looks a little like configuration
settings and a little like a programming language?

#### An edge case

Some inputs have aspects of both data (settings) and programming.  Configuration
files can start out containing only data (settings, simple declarations), but
over time grow to include some aspects of programming languages like variables
and conditionals, and occasionally actual control structures.

The [dockerfile](https://docs.docker.com/engine/reference/builder/) is a
contemporary example of a configuration file that started out with a simple
format of declarations mixed with `RUN` commands.  Over time, it has grown to
include two kinds of variables, multiple execution phases, and directives that
change how the file is parsed.  What was once a file so simple that it might
have been processed by trivial lexer (a handful of regex defining tokens) and a
few lines of code, today it is parsed by a trivial lexer and
[maybe 500 lines of code](https://github.com/moby/buildkit/tree/master/frontend/dockerfile/parser).
The 2019 dockerfile format is on the edge, perhaps, of needing a "proper
parser".  It's prefix format, however, in which each line starts with a
declaration or command, makes it possible to read the first token on a line and
dispatch to a different hand-made parser for each initial token.

The processing of a dockerfile, then, is done without an explicit grammar.  The
hand-written code for each type of line implicitly defines a grammar rule for
that line.

Programming languages in the Lisp family have a similar character.  Their
essential structure is trivial: a *form* is an atom or a parenthesized list of
forms.  Atoms are things like numbers, strings, and symbols.  Furthermore, the
syntax is designed to have unique prefixes.  That is, each kind of form is
distinguished by its first character or two, which means it is easy to write a
basic Lisp parser by hand in a few dozen lines of code.  Adding support for
macros, Unicode, and other features is straightforward.

Lisp's syntax, then, is another case for which a simple hand-written parser
suffices.  (In fact, many Lisps do not use a separate lexer at all, instead
hand-writing lexical analysis interleaved with parsing.)  No separate parsing
tool (e.g. from the Parsing examples in <a href="figure-1">Figure 1</a>) is
needed.

Most programming languages have much more complex structure than Lisps, though.
And while dockerfiles contain script-like elements (variables, sequences of
commands), most scripting languages are (these days) as complex as traditional
programming languages (though none are as
[complex as C++](http://blog.reverberate.org/2013/08/parsing-c-is-literally-undecidable.html)).


#### Little languages

You may have heard the phrase "little languages".  It refers to a Jon Bentley
[Programming Pearls column](https://dl.acm.org/citation.cfm?id=315691&dl=ACM&coll=DL)
promoting the virtues of small, custom languages for describing data and
procedures.  Bentley "first learned the importance of little languages from
Mary Shaw, who edited The Carnegie-Mellon Curriculum for Undergraduate
Computer Science (published by Springer-Verlag in 1985)."

In his 1986 column, Bentley stresses the utility of little languages that are
tailored to a single problem domain, such as drawing pictures of chemical
compounds ([CHEM](https://web.cecs.pdx.edu/~trent/gnu/groff/122.ps) and
[relatives](https://en.wikipedia.org/wiki/Troff#cite_note-18)) or designing
survey questions.

We encounter such domain-specific languages often in software development.  A
common one is the language of the "format string" used to generate
formatted text
(e.g. [printf](https://en.wikipedia.org/wiki/Printf_format_string)).  Sure, it's
a simple language, where most characters are self-representing literals and
a sequence like `%4.2f` encodes instructions for formatting a floating point number.

Really simple languages like the one used by `printf` can be processed without a
lexer or parser.  A modest loop for dispatching and a few static tables would
probably work.  The
[actual implementation](https://sourceware.org/git/?p=glibc.git;a=blob;f=stdio-common/vfprintf.c;h=fc370e8cbc4e9652a2ed377b1c6f2324f15b1bf9;hb=3321010338384ecdc6633a8b032bb0ed6aa9b19a)
of `vprintf`, in GNU glibc, consists of about 1000 lines of macros, a jump
table, and a few functions made inscrutable by conditional compilation
directives.  Presumably, the cost paid in maintainability
[has reaped savings](https://www.oxfordlearnersdictionaries.com/us/definition/american_english/sarcasm)
in the form of clock cycles saved by a faster `printf`.


But I digress.  

My point about
[domain-specific languages](https://en.wikipedia.org/wiki/Domain-specific_language)
is that simple ones may be processed easily with hand-written code or a lexer,
which are pattern matching techniques.  A DSL with more complex structure is
typically processed using a parser.  The languages mentioned by Bentley, like
CHEM, are parsed using [lex](https://en.wikipedia.org/wiki/Lex_(software)) (a
lexer generator) and [yacc](https://en.wikipedia.org/wiki/Yacc) (a parser
generator).

Bentley's "little languages" column illustrated how easily tools like lex and
yacc could be used (if you were a C programmer in 1986).  Such tools were
invented for writing compilers for "big languages" like
[B](https://en.wikipedia.org/wiki/B_(programming_language)) and C itself, and
later many more.  This brings us, fortuitously, to the topic of processing
programming language source code.  We start by looking at "scripting"
languages. 

#### Scripting languages

Suppose you have a small language that needs to grow up a bit.  Maybe there is a
requirement for new value types, or for function definitions.  Your little
language probably has an interpreter, and you want to add to it a scripting
capability.  This is well-traveled territory; here are some examples:

* [dockerfile](https://docs.docker.com/engine/reference/builder/): contains
  configuration (declarations) and command sequences to build a docker container;
* [jenkinsfile](http://jenkins.io/doc/book/pipeline/#pipeline-syntax-overview):
  specifies a pipeline for staged execution of build tasks;
* [vimscript](http://learnvimscriptthehardway.stevelosh.com): enables vim users
  to set editor options for customization, as well as to write custom scripts in
  a [much discussed custom scripting language](https://www.reddit.com/r/vim/comments/54224o/why_is_there_so_much_hate_for_vimscript/)

In my observations over a programming career that began nearly 37 years ago (as
of this writing, in 2019), scripting capabilities spring up like
[volunteer trees](https://en.wikipedia.org/wiki/Volunteer_(botany)) when
conditions are right.  The seed is rather small, perhaps a file in which one can
set her editor preferences.  The soil is amenable when the application itself
(e.g. an editor) is a complex program.  Finally, the needed sunshine and rain
appear as the user base for the application grows and some users make a
passionate case for scriptable configuration.

When something "scriptable" grows, its sapling form usually resembles a linear
sequence of commands.  Then variables and conditionals sprout.  As they absorb
the sunshine radiating from happy users, they also feel the stinging rain of
"this is not enough".  It's hard to know what is the right amount of rain.  Too
much, and oddly-shaped control structures grow out sideways, curling this way
and that around domain concepts like buffers and build stages and environments.
The scripting language takes on an unfamiliar shape, where from one angle it
looks like general programming language, and from another angle it is quite
domain specific.

<!-- Pic of tree growing around an object -->
<img width="100%" 
	 src="{{ site.baseurl }}/images/TreesEating.png"
	 alt="A collage of 4 photos.  In each one, a tree has grown around one of
	 the following: a gravestone, a sign that reads &quot;Road Closed&quot;, a
	 metal railing, and another tree.  The intertwined trees form a large square
	 knot, and were obviously forced into this configuration as the trees grew."/>

There is nothing inherently wrong with scripting languages that are shaped
around elements of their domain.  In practice, though, the more a language looks
like a traditional (general) programming language, the more users will expect it
to behave like one.  For instance, if you can define functions, then programmers
will expect function variables to be lexically scoped. (Perl 5 has infamously
complicated and unique scoping rules.)  If you have conditionals, then equality
testing behaves in a predictable way.  (Javascript is a language with bizarre
equality operators, one of which is not transitive!  There are many
[explained examples](https://github.com/denysdovhan/wtfjs) available.)

We have taken the scenic path to arrive at the following point: languages grow.
As they take on new features, their syntax usually changes, and therefore the
code that processes the source code (the parser) must change.

In the last four decades or so, language implementors have found it useful to
write a formal grammar for their scripting language, and then use either a
parsing library or a parser generator.  The effort to modify the lexer rules or
the parser rules as the language changes has been widely regarded as low
compared to modifying a hand-written lexer and parser.

Returning to your small language that needs to grow, you may want to change from
a simple hand-written parser to using a parsing library (or parser generator) as
the structure of your language grows more complex.  The benefits we describe in
the next section accrue when processing a language that is any more complicated
than the current dockerfile or jenkinsfile, in my opinion.

#### Programming languages

The line between scripting languages and general purpose programming languages
is blurry.  One language, [Lua](https://www.lua.org), comfortably inhabits the
blurry space by design.  The Lua authors set out to write a general purpose
language that can be easily embedded into another application to provide that
application with scripting capability.  The interface between Lua and a "host
application" is especially elegant.  (See,
e.g. [this short essay](https://queue.acm.org/detail.cfm?id=1983083).)

But that is beside the point, which is that whether a language is considered a
scripting language or not, it almost certainly has a proper parser in its
implementation.  So you may be writing a DSL like `CHEM`, a scripting language
like `vimscript` or `Lua`, or a compiler for C, Go, Haskell, or Rust.  In all of
these cases, pattern matching will not get you far.  Well, it will get you as
far as lexical analysis, transforming source code into a sequence of tokens.
Then you'll need a parser to turn those tokens into a parse tree.

Referring back to <a href="figure-1">Figure 1</a>, any of the example tools in
the Parsing region of the diagram might do, or you can choose from dozens of
others.  But, how to choose?

Compiler and interpreter writers often want to accomplish other tasks during
parsing, such as building symbol tables.  This is driven mostly by a desire for
efficiency, to avoid processing the same input multiple times.  It also
facilitates the production of useful error messages.  We talked already about
parsing as an activity that requires a grammar and an input string, and produces
a parse tree (upon success).  How would we tell a parser to also construct a
symbol table, or to do other computations while parsing?

One solution is to use a parser generator, which takes a grammar as an argument
but also fragments of code to be executed at particular times while parsing.
The parser generator then outputs the source code for a parser, and within that
source code can be found the code fragments that do the custom computations.
The [yacc](https://en.wikipedia.org/wiki/Yacc) tool does this for the C
language: the grammar definition it consumes contains fragments of C code, and
the output of yacc is C source code.  

Bison is a GNU project that is roughly comparable to yacc, which is quite old.
Yacc has many other descendants as well, in spirit if not in code.  ANTLR is a
lexer/parser generator whose output can be Java, C++, Python, or a number of
other languages.

Another solution is to use a parsing library like the Haskell package
[parsec](https://hackage.haskell.org/package/parsec).  Here, the library helps
you build a parser in your Haskell program.  So, if you were writing a
`vimscript` interpreter in Haskell (for some mysterious reason), you might start
by writing a grammar for `vimscript` on paper.  Next, write some Haskell code
that uses parsec to implement that grammar.  

Because parsec is so well designed, your code will bear a strong resemblance to
the grammar you wrote down.  And because you wrote your parser in Haskell,
parsing code can mix with other code to do arbitrary computations during
parsing.  You can build symbol tables, output log information, generate fancy
error messages, or pretty much anything else.

[Marpa](https://savage.net.au/Marpa.html) is a parsing library, written in C
with a Perl wrapper and bindings for some other languages.  The goal of this
project is to provide the ultimate parser library: fast and powerful.  If the
language bindings live up to this goal, it should be possible to do with Marpa
the kinds of things one can do with parsec, including interleaving arbitrary
computation with parsing steps.

The Perl 6 grammar feature is an attempt to provide similar capabilities for
users of Perl, although Perl 6 is a new project distinct from Perl 5 and not
necessarily compatible.  Perl's creator has
[reportedly quipped](https://en.wikipedia.org/wiki/Perl_6#cite_ref-12) that "it
would be better to fix the language than fix the user."  In this case, the fix
is a full redesign and re-implementation.  The mascot for Perl 6 is (I am not
making this up) fittingly, a bug.

What does all of this talk about parsing programming languages add up to?  The
technique for modeling such languages is most often a context-free grammar, and
the terminology we use is that the input string is *parsed* using a grammar.  As
tools, parsers have developed along two primary routes (parsing libraries and
parser generators), both of which allow the intermixing of arbitrary other code
with parsing.

In sum, the use case for processing artificial languages like programming
languages has been addressed over the decades by parsing libraries and parser
generators, designed to recognize input strings using a grammar and to perform
other computations interleaved with parsing.  That is, parsing tools are
designed for compiler and interpreter writers.

<!-- ----------------------------------------------------------------------------- -->
## The middle ground

A number of technologies do not fit well into either category, being part each
of Pattern Matching and Parsing.  In our Venn diagram (<a href="figure-1">Figure
1</a>) we show two: PEG/RPL and PCRE (Regex).  PEG stands for
[Parsing Expression Grammar](https://en.wikipedia.org/wiki/Parsing_expression_grammar),
and it's the formal basis for [Rosie Pattern Language](https://rosie-lang.org)
and other projects.  As the creator of Rosie/RPL, I want to discuss what it is
good for, and where I expect it to fall short.  But first, let's look at PCRE.

#### PCRE

PCRE stands for
[Perl Compatible Regular Expressions](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions),
but there are some differences between the two.  Although it has Regular
Expressions in its name,
[PCRE supports](https://www.pcre.org/current/doc/html/pcre2syntax.html)
subroutines and recursion.  PCRE may not be as powerful as Perl 6 grammars, but
subroutines, recursion, conditionals, back references, and look-arounds give it
lots of expressive power.  Plus, a
[callback mechanism](https://www.pcre.org/current/doc/html/pcre2callout.html)
allows arbitrary user code to be run during the matching (parsing?) process.
PCRE incorporates features from Perl, Python, .NET, and Oniguruma, as well as
unique features of its own.

PCRE is a very comprehensive tool in the pattern matching category.  Is PCRE
ever chosen as a parsing tool for a simple data format or a little language?
I'm sure it is.  Is it a suitable parser for programming languages?  Not in my
opinion.  Even though PCRE supports a large class of patterns (beyond regular
expressions), and even though I can configure callbacks into my own code, it was
not designed as a parser.

PCRE expressions are built with regex syntax -- dense and cryptic.  The word
*grammar* does not even appear in the
[PCRE syntax document](https://www.pcre.org/current/doc/html/pcre2syntax.html),
even though it talks about subroutines and recursion.  It is not a criticism to
say that PCRE is unsuitable as a parser, because it was not designed to be one. 

As a flexible regex matching tool, PCRE shines.  Perhaps it shines a bit too
brightly though.  Users can be blinded by all of the PCRE options that they
can't see where their regex patterns could go wrong.  As of version 10.33, there
are (give or take):

  * 5 PCRE build-time options
  * 30 regex compile-time options
  * 10 regex match-time options

I only counted options that affect the semantics of a regex, i.e. what it
matches (and captures) and what it does not.  The
[PCRE pattern documentation](https://www.pcre.org/current/doc/html/pcre2pattern.html)
contains the word "if" 240 times, many of which are in statements like these:

	"It is possible to restrict \R [...] by setting the option PCRE2_BSR_ANYCRLF"
    "If the PCRE2_EXTRA_ESCAPED_CR_IS_LF option is set..."
	"if PCRE2_NOTBOL is set, circumflex can never match if the PCRE2_MULTILINE option is unset"
	"... is recognized only when the PCRE2_UTF option is set"

These options all potentially interact, and some of them can be altered from
within the regex pattern, while the others are passed as flags through the API
(and the build-time options are fixed when PCRE itself is compiled).  The sheer
number of special cases is overwhelming to me, and I briefly considered writing
an analysis tool to help me find errors related to these options before code
goes into production.  An obstacle to static analysis here is the need to know
not only the build options and the regex patterns themselves, but also which
flags are passed at regex compile-time and at regex match-time.

For human users of PCRE (and many other regex implementations), the conditional
effects of options that are specified outside the pattern itself compounds an
existing problem with regex: there are already lots of exceptions in regex.  For
example:

  * Whether a character is a literal or a meta-character in a "character class" (a
    square-bracketed expression like `[a-z]`) depends on its position,
    e.g. `[+-/*]` will match a comma (`,`).
  * The previous exception has an exception: `[ab]46]` will (silently) be
    interpreted as "either `a` or `b`, followed by `46]`, not a character class
    containing 5 characters.
  * Back references can be confused with single characters written as octal
    escapes, e.g. `\11`.
  * What is matched by `^` and `$` depends on the current mode/option.
  * `[:digit:]` matches any of `:`, `d`, `i`, `g` or `t` without warning or
    error. 
  * Parentheses are used for grouping and to denote captures; and with a `?` to
    introduce a look-around, to change an option, to write a conditional, to
    call a subroutine, to start a comment, and many other things.
  * Whether dot (`.`) matches a single character (and what is a single
    character) depends on several options/modes.

To be clear, the list above (which is certainly not exhaustive) is not a PCRE
issue.  It is a regex issue, inherited by PCRE and other regex libraries.
Informally, the number of things a person needs to keep in mind while doing a
task is called *cognitive load*.  When writing, maintaining, or debugging regex,
the cognitive load is high.  It may not appear that way at first, but it comes
home to roost later when an expression does not work as expected.

And, you might think that regex mistakes would be caught in testing, but often
this is not the case.  A researcher in my department (Computer Science, North
Carolina State University) estimates that less than 17% of regular expressions
in programs are covered by unit tests, and that only a small fraction of those
tests cover both matching and non-matching cases.

Let's sum up.  With good performance, tremendous flexibility, and over 20 years
of development, PCRE is a strong choice for many projects, including many
high-profile ones.  However, there are costs in development time and maintenance
for any solution that layers dozens of "rules with exceptions" on top of an
expression language that itself has many "rules with exceptions".  Regular
expressions are simply not very regular.

<!-- PCRE Design issues: -->

<!-- 1. "when PCRE2's Unicode support is enabled, the data must be valid UTF-8" -->
<!--    https://github.com/BurntSushi/ripgrep/blob/master/FAQ.md#pcre2-slow -->
<!-- 2. regex syntax (yes can have whitespace and comments, but still.) -->
<!-- 3. options change SEMANTICS, so they limit static analysis oppty -->
<!-- 4. subroutines and recursion added without grammars, so these features are (imo) -->
<!--    hard to understand -->


<!-- 	"passing the PCRE2_NOTEMPTY or PCRE2_NOTEMPTY_ATSTART option..." -->
<!--     "If the PCRE2_NEVER_UTF option is passed to pcre2_compile()..." -->
<!-- 	"If a pattern is compiled with the PCRE2_EXTENDED option..." -->
<!-- 	"If PCRE2_ALT_BSUX is set, the sequence \x followed by { is not recognized." -->
<!-- 	"... is recognized only when the PCRE2_UTF option is set" -->
<!-- 	"If the PCRE2_EXTRA_ESCAPED_CR_IS_LF option is set..." -->
<!-- 	"However, if either of the PCRE2_ALT_BSUX or PCRE2_EXTRA_ALT_BSUX options is set..." -->
<!-- 	"the definition of letters and digits [...] vary if locale-specific matching is taking place" -->
<!-- 	"In 8-bit, non-UTF-8 mode..." -->
<!-- 	"It is possible to restrict \R [...] by setting the option PCRE2_BSR_ANYCRLF" -->
<!-- 	"see the discussion of PCRE2_NO_UTF_CHECK" -->
<!-- 	"When PCRE2 is built with Unicode support, the meanings of \w and \W can be -->
<!-- 	changed by setting the PCRE2_UCP option. When this is done, it also affects -->
<!-- 	\b and \B."  -->
<!-- 	"if PCRE2_NOTBOL is set, circumflex can never match if the PCRE2_MULTILINE option is unset" -->
<!-- 	"The meaning of dollar can be changed [...] by setting the PCRE2_DOLLAR_ENDONLY option" -->
<!-- 	"The meanings [...] are changed if the PCRE2_MULTILINE option is set. [...] -->
<!-- 	However, this can be changed by setting the PCRE2_ALT_CIRCUMFLEX option." -->
<!-- 	"The escape sequence \N [...] behaves like a dot, except that it is not -->
<!-- 	affected by the PCRE2_DOTALL option." -->
<!-- 	"if the PCRE2_ALLOW_EMPTY_CLASS option is set..." -->
<!-- 	"The settings of the PCRE2_CASELESS, PCRE2_MULTILINE, PCRE2_DOTALL, -->
<!-- 	PCRE2_EXTENDED, PCRE2_EXTENDED_MORE, and PCRE2_NO_AUTO_CAPTURE options can -->
<!-- 	be changed from within the pattern..." -->
<!-- 	"The duplicate name constraint can be disabled by setting the PCRE2_DUPNAMES option" -->
<!-- 	"If the PCRE2_UNGREEDY option is set [...] In other words, it inverts the default behaviour." -->
<!-- 	"This feature can be disabled by the PCRE2_NO_AUTOPOSSESS option" -->
<!-- 	"if the PCRE2_MATCH_UNSET_BACKREF option is set at compile time..." -->
<!-- 	"If the PCRE2_EXTENDED or PCRE2_EXTENDED_MORE option is set..." -->
<!-- 	"When PCRE2_ALT_VERBNAMES is set..." -->
<!-- 	"set the PCRE2_NO_START_OPTIMIZE option..." -->
	
<!-- * PCRE needs nearly 250 (safe to say OVER 200) "if" statements to describe syntax! -->
<!-- * PCRE has 6 or 7 build options that affect semantics -->
<!-- *  -->
<!-- * libpcre2-8.0.dylib is about 500KB -->
<!-- * librosie.dylib is about 260 KB, projected run-time-only size is 75KB -->


#### Rosie/RPL

Now we switch from ranting to raving.  After all, I designed RPL and implemented
Rosie for exactly the reasons unpacked in this essay:

1. Regex are hard to write correctly, to test well, and to maintain.
2. When a project needs many regex, it also needs a way to organize them.
3. Regex are no longer based on the regular expression theory.  
   * I would like a solution grounded in a formalism, that we might use the
	 formal theory to prove properties of our system.
   * Regular expressions have proven insufficient for pattern matching use
     cases, so we should adopt a more powerful approach.
4. Regex implementations can perform exponentially badly in edge cases.  But
   when you have a large collection of patterns and big data to process, those
   edge cases do occur -- and we need to prevent them.
   
Well, that last item is something we have not mentioned here.  Instead of
burdening this essay with one more topic (a.k.a. "the straw"), we will leave
that for a future rant.

!@#


#### A note about concision

I began this essay by noting that regular expressions came into use initially as
a practical tool for text search.  That use case put a high value on concision,
meaning that expressions should be very short.  For interactive use, where a
regex is written on the fly during text editing or on the command line, a
concise, compact notation is important.

This was especially important in the 1970's and 1980's when so much computer
access was through terminals.  Each key press went over a dedicated wire from
the terminal to a serial port on a computer with very little processing power or
memory by today's standards.  (From about 1983-87, I had a job writing DIBOL on
a PDP 11/44.  This was a 16-bit machine with 20-bit addressing, and ours had
512KB of memory.  It served around 15 concurrent users on VT-220 terminals, and
with the two [CDC 80MB](https://en.wikipedia.org/wiki/Storage_Module_Device)
removable-media hard drives probably cost USD$50,000 in 1980, or USD$150,000
today.) 

Needless to say, we did not have editors that provided auto-completion, nor
anything like an IDE.  (I wrote code using
[EDT](https://en.wikipedia.org/wiki/EDT_(Digital)), fwiw.)  Anything that saved
keystrokes was considered a good thing. 

Fast forward to 2019.

Individual keystrokes are not so precious now.  Shells remember commands we
typed, and we can edit them for reuse.  Both shells and editors auto-complete.
And IDEs save key presses, if not time, using buttons and menus.



Parsing:
- patterns (grammars) may be complex, e.g. CFG or CSG
- result is a parse tree
- often a "second pass" after input text has been tokenized by pattern matching
- other computations are interleaved with parsing
- natural languages have their own rules?
- artificial languages have rules like operator precedence and associativity

Pattern matching:
- patterns (expressions) usually simple, e.g. regular or subset of CFG
- result is a list (of matches or captures), i.e. it is linear
- often a "first pass" before parsing, except for "scanless parsing"
- no other computations interleaved (so easy to make it a cross-language
  library)
  
  




## Notes

#### Note 1

There is much to be said about the ways that a regular expression can be
transformed (compiled), what forms it can take, and how those forms behave
during execution.  Common forms are NFA and DFA (non-deterministic and
deterministic finite automata, respectively), each with its own advantages and
downsides. 
<br>
<a href="cite-note-1"><b>Return</b></a>

#### Note 2

Some ways that regex syntax can be used to control the matching algorithm
include:

* whether or not the matching is greedy;
* whether sub-expressions are *possessive*;
* if letter case matters (typically only for ASCII text);
* which characters are matched by `.` (dot), `$` (dollar), and `^` (caret); and
* the option to match ahead or behind without "consuming" input (known as *look
  ahead* and *look behind*).

Each one except the first (greediness) changes the set of strings recognized by
the expression, i.e. they change the semantics.  The last item, *look arounds*,
enhance the expressive power of patterns, making it possible to write patterns
that recognize languages that are not regular.  Back references give expressive
power as well.  Other features, such as case sensitivity, are merely
conveniences that make it easier to write recognizers for regular languages. 

<br>
<a href="cite-note-2"><b>Return</b></a>

#### Note 3

A proof that an input string does not belong to the described language may not
appear to have any use.  At run-time, during text processing, this may be true.
But during development, the details of such a proof can be an invaluable aid to
debugging language descriptions like regex or grammars.  The Rosie Pattern
Language implementation has a `trace` function that produces such a proof, drawn
as a tree.  In particular, the tree nodes representing choices demonstrate the
attempted recognition of each alternative, and constitute a case-based proof.

<br>
<a href="cite-note-3"><b>Return</b></a>


<hr>

Follow Dr. Jamie Jennings on [Twitter](https://twitter.com/jamietheriveter) for
Rosie announcements and other miscellanea.
