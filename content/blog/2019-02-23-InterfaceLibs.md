---
layout: blog
title:  "Rosie Interface Libraries"
categories: 
lastmod: "2019-02-23"
---

The first release candidate for Rosie v1.1.0 is in the
[dev branch](https://gitlab.com/rosie-pattern-language/rosie/tree/dev) of the
Rosie repository on GitLab. Among other changes, we have moved the code that
interfaces between `librosie` and languages like Python, Go, C, and Haskell.
Those interface libraries are now in the
[Clients](https://gitlab.com/rosie-community/clients) subgroup of the
[Rosie Community](https://gitlab.com/rosie-community) group.

Please try out these clients if you program in one of the supported languages!
The clients should work with the current Rosie v1.0.0 as well as the forthcoming
v1.1.0.  Thanks in advance for bug reports, enhancement suggestions, and pull requests!

## Repository organization

### Rosie Pattern Language group on GitLab

The [main Rosie repository](https://gitlab.com/rosie-pattern-language/rosie)
contains the CLI, `librosie`, and `librosie.h`.  These are built from source by
running `make`, and installed with `make install`.

The default destination, `/usr/local`, is customized by setting `DESTDIR`,
e.g. 

```shell 
make install DESTDIR=/opt
``` 

Both the CLI, `DESTDIR/bin/rosie`, and `librosie` require the files in
`DESTDIR/lib/rosie` to run.

In v1.0, this repository also contained the interface libraries needed to write
programs that use `librosie`. We've been calling these libraries (like
`rosie.py`) *clients* of `librosie`.  Including them in the same repository as
Rosie itself was a temporary convenience, and it's time to move them out on
their own.

Note: The other repositories in the Rosie Pattern Language group are submodules
of the Rosie project.  These will undergo some simplification in the coming
months, but their structure should be irrelevant to Rosie users.

### Rosie Community group on GitLab

The repositories in the [Rosie Community](https://gitlab.com/rosie-community)
group are meant to be maintained by users/contributors.  Today there are a few
repositories there:
* [rawdata](https://gitlab.com/rosie-community/rawdata): a small collection of
  patterns for destructuring raw text data.
* [lang](https://gitlab.com/rosie-community/lang): patterns for extracting
  interesting features from source code in a variety of languages.
  Inexplicably, the README contains instructions on how to create and use a
  `~/.rosierc` file.  (This should be part of the Rosie documentation.)
* [Packages](https://gitlab.com/rosie-community/packages) is a group of
  different packagings of Rosie:
  * [PyPI](https://gitlab.com/rosie-community/PyPI)
  * [homebrew-rosie](https://gitlab.com/rosie-community/homebrew-rosie)
* [Clients](https://gitlab.com/rosie-community/clients) is a group of
  repositories for `librosie` clients, so that Rosie can be used in Python,
  Haskell, Go, and other languages.

<span style="color:darkviolet">**Edit August 15, 2023:** A talented and generous
contributor created a [Rust interface](https://lib.rs/crates/rosie) to Rosie.

As you can infer from the list above, the Rosie Community repositories can be
used to share RPL packages, to collect various packaging recipes, and to collect
the `librosie` client libraries.


## Current status

As of 23 February 2019, we have a first release candidate for v1.1.0 at the HEAD
of [the dev branch](https://gitlab.com/rosie-pattern-language/rosie/tree/dev).
This release is the first to have the `librosie` client libraries in separate
repositories. 

If you would like to run the bleeding edge version of Rosie, please try out this
release candidate.  And please try the various client libraries.  We welcome
issues to report bugs or suggest enhancements, and pull requests as well.


<hr>

We welcome feedback and contributions.  Please open issues (or merge requests)
on [GitLab](https://gitlab.com/rosie-pattern-language/rosie), or [get in touch
by email](mailto:info@rosie-lang.org).

<span style="color:darkviolet">**Edit August 15, 2023:** You can find my contact
information, including Mastodon and LinkedIn coordinates, on [my personal
blog](https://jamiejennings.com).


