---
layout: post
title:  "Release plan update"
date:   2019-01-11
categories: 
lastmod: "2019-01-11"
---

Our [last post](/blog/2018-12-08-release-plan)
described a released planned for the end of 2018, which has been delayed.  To
avoid unnecessary churn, we are taking this opportunity to move the Rosie/RPL
interface libraries (in Python, Go, C, and Haskell, with more forthcoming) to
their own repositories.  This move will facilitate development of those
interfaces as well as new ones, independent of the Rosie/RPL release plan.

The Rosie v1.1.0 release is now planned for the end of March.


## Release v1.1 plan contents

Recent contributions include:
* [Haskell library](https://whatthefunctional.wordpress.com/2018/10/10/making-a-haskell-interface-for-the-rosie-pattern-language/)
* (Forthcoming) Python library for Rosie that presents a very similar API to
  the existing [re module](https://docs.python.org/3/library/re.html).
* An RPL library which redefines `.`, `^`, and `$` such that:
    * Dot `.` will not match newline
	* Caret `^` will match the start of input or newline
	* Dollar `$` will match the end of input or newline 
<ul style="list-style-type: none; margin-top: -16px; margin-bottom: -16px;">
<em>This library is imported like any other RPL library, and provides the
ability to use these alternate definitions of builtins within the scope of
the RPL code that imports it.</em>
</ul>
* Install script for Windows Subsystem for Linux

Functional enhancements:
* The `librosie` ability to measure the time needed to perform a match will be
  exposed through the CLI.  Two metrics will be reported, both are CPU time used
  by the Rosie process (user time) and are reported in microseconds: time spent
  within the matching vm, and total time.  The total time is the sum of the time
  in the matching vm and the time needed to encode the output.

Internal changes:
* In preparation for ahead-of-time compilation of arbitrary RPL patterns written
  by users, as well as the ones in the standard library, the patterns needed by
  Rosie internally have been pre-compiled.  This set of pre-compiled patterns
  will be loaded from files at startup instead of being compiled from RPL
  source.
* Ahead-of-time compilation, when delivered in a 2019 release, will reduce
  startup time, according to tests already conducted.  It will also enable the
  Rosie binary to be split into a compiler component and a run-time component.
  When matching against a pre-compiled pattern, only the run-time binary is
  involved.  Early experiments suggest that this binary will be less than 100Kb
  in size.  The next Rosie release will be based on the refactoring that we have
  done to prepare for separate binaries.

In summary, the coming Rosie v1.1.0 release will contain fixes for reported
bugs, several user contributions, one functional enhancement, and some internal
changes. 

## Questions and feedback are welcome

<hr>

We welcome feedback and contributions.  Please open issues (or merge requests)
on [GitLab](https://gitlab.com/rosie-pattern-language/rosie), or [get in touch
by email](mailto:info@rosie-lang.org).

<span style="color:darkviolet">**Edit August 15, 2023:** You can find my contact
information, including Mastodon and LinkedIn coordinates, on [my personal
blog](https://jamiejennings.com).  The mailing list
[https://groups.io/g/rosiepattern](https://groups.io/g/rosiepattern) has fallen
out of use since we mostly use Slack, but perhaps it will be revived.




