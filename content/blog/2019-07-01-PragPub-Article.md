---
layout: blog
title:  "PragPub Article on TDD with RPL"
categories: presentation
lastmod: "2019-07-01"
---

<!-- <p style="background-color:#f0f0f0;line-height:200%">  -->
_Edited Sunday, July 21, 2019: With permission of the author, the complete
article is available [here](/pres/TDDWithRosie.pdf)._
<!-- </p> -->

<hr>


See the May 2019
[contents of PragPub](https://theprosegarden.com/contents-of-recent-issues/#06-19)
for an article by Onorio Catenacci on TDD using Rosie Pattern Language!

Because Rosie has built-in executable unit tests, you can develop patterns
iteratively, which is usually hard to do with regular expressions.  Onorio gives
a step by step example.

<hr>

We welcome feedback and contributions.  Please open issues (or merge requests)
on [GitLab](https://gitlab.com/rosie-pattern-language/rosie), or [get in touch
by email](mailto:info@rosie-lang.org).

<span style="color:darkviolet">**Edit August 15, 2023:** You can find my contact
information, including Mastodon and LinkedIn coordinates, on [my personal
blog](https://jamiejennings.com).  The mailing list
[https://groups.io/g/rosiepattern](https://groups.io/g/rosiepattern) has fallen
out of use since we mostly use Slack, but perhaps it will be revived.

