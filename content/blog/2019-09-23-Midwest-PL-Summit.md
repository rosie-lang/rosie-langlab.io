---
layout: blog
title:  "Midwest PL Summit"
lastmod: "2019-09-23"
---

My slides from the Midwest Programming Languages Summit, hosted by the PL group
at Purdue University, are online [here](/pres/RosiePatternLanguage-MWPLS-2019.pdf).

<hr>

We welcome feedback and contributions.  Please open issues (or merge requests)
on [GitLab](https://gitlab.com/rosie-pattern-language/rosie), or [get in touch
by email](mailto:info@rosie-lang.org).

<span style="color:darkviolet">**Edit August 15, 2023:** You can find my contact
information, including Mastodon and LinkedIn coordinates, on [my personal
blog](https://jamiejennings.com).  The mailing list
[https://groups.io/g/rosiepattern](https://groups.io/g/rosiepattern) has fallen
out of use since we mostly use Slack, but perhaps it will be revived.

