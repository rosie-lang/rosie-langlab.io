---
draft: true
title:  "Parsing infix with parser combinators"
description: "DRAFT"
lastmod: "2019-06-11"
---

In a [related post]({{ site.baseurl }}{% post_url
2019-06-10-To-Parse-or-Not-to-Parse %}), I talked about using Rosie as a parser.
While there are many parsing tasks that are easily handled by Rosie (or any PEG
parser), there are pitfalls to parsing complicated languages, like programming
languages, this way.  This post addresses such a pitfall: parsing infix
expressions. 

A worst-case performance issue arises here that is common to most approaches
that use [_parser combinators_](https://en.wikipedia.org/wiki/Parser_combinator),
with which it is possible to observe run-times that are exponentially long in the
length of the input.


<!-- ----------------------------------------------------------------------------- -->
## Combinators

Here we will consider a _combinator_ to be a function that combines other
functions to produce a new one.  A _parser combinator_, then, is a way of
combining parsers to produce a new one, where we model a parser as a function
from text (the input) to a parse tree (the output).

RPL is a language of combinators.  Every RPL expression is a function from text
to a parse tree.  Expressions are built from literals, like fixed strings and
character sets, and by combining other expressions.  The simplest way to combine
two expressions is to put them in sequence: a sequence of two RPL expressions,
`{p q}` is a new expression.  Since each of `p` and `q` is a parser, the
RPL sequence operator (adjacency) is a parser combinator.  The RPL language
consists of these essential operators:

* Sequence: `{p q}`
* Choice: `p / q`
* Repetition: `p*`, `p+`, `p?`, and `p{n,m}`

These combinators are close relatives of the combinators that create regular
expressions <small><a name="cite-note-1">\[[Note 1](#note-1)\]</a></small>.

RPL, like other Parsing Expression Grammars, also supports recursion.  The
syntax of RPL was designed to highlight recursion, so that accidental recursion
cannot occur.  A `grammar...end` block denotes a set of RPL bindings that may
be recursive.  (The way bindings occur in such a block is similar to `letrec` in
Scheme.)

<!-- A note about macros here?  About backreferences? -->

<!-- ----------------------------------------------------------------------------- -->
## Recursive descent

There are many algorithmic approaches to parsing.  Parser combinators are one of
a family of approaches called
[recursive descent](https://en.wikipedia.org/wiki/Recursive_descent_parser)
parsing.  The other prominent family of parsing algorithms is called _LR_, of
which the _LALR_ (Look-Ahead LR) may be the best known.  The _LR_ parsers are
guaranteed to run in linear time in the input length, but they are not easy to
implement.  As a result, such parsers are created using _parser generators_.
The input to a parser generator is a grammar, much like a BNF grammar or an RPL
`grammar`.

In practice, parser generators are used by attaching code fragments to grammar
rules (also called _productions_).  A tool processes the grammar and produces
source code.  E.g. Yacc produces C code and ANTLR can produce code in Java, C#,
Go, and other languages.  Code fragments written by the user to accompany the
grammar are inserted into the code that is produced.  In this way, custom
actions written by the user are executed during parsing.

By contrast, recursive descent parsers are considered easy to write -- at least,
if they are implemented using backtracking.  A backtracking parser tries each
grammar rule until one succeeds.  For instance, suppose at input position `i`
there are two rules that might apply, `X` and `Y`.  The parser tries `X`,
consuming input beyond position `i` until `X` either succeeds or fails.  If it
fails, the parser rewinds the input to position `i` and tries `Y`.

(This is exactly what happens when Rosie encounters the expression `X / Y`.  The
current input position is pushed onto a stack, and `X` is attempted.  If it
fails, the input position is reset back to `i` by popping the stack, and then
`Y` is attempted.)

Backtracking means that a portion (or all) of the input may be parsed many
times.  Even exponentially many times!  (That is the subject of the next
section.)  Some implementations of recursive descent use _memoization_ to
guarantee linear time performance.  (The "packrat parser" discussed in the
[original PEG paper](https://pdos.csail.mit.edu/~baford/packrat/thesis/) is an
example.)  The tradeoff, as usual, is space for time -- packrat parsers require
lots of memory.

_Predictive parsers_ are another implementation of recursive descent, in which
the grammar is transformed such that, with `k` (a constant) tokens of
look-ahead, the correct grammar rule can be chosen, thus avoiding backtracking.
The result is linear-time performance, but only when the grammar can be
transformed appropriately -- and this is not a trivial task.  However, when
designing a language it is possible to arrange things such that one token of
look-ahead (`k` = 1) is sufficient.

Tools like Rosie, which can clearly perform parsing, are designed to accept a
wide variety of grammars as input, i.e. as RPL patterns.  Some grammars,
probably most, will perform well, with linear time on typical input.  Many will
even perform well on adversarial input, meaning under a worst case analysis.
(This is important for preventing denial-of-service attacks by adversaries who
are able to craft a worst-case input string, knowing the nature of the parser.)

But some grammars will take exponential time on worst-case input strings.  We
will look at such a grammar in the next section.


<!-- ----------------------------------------------------------------------------- -->
## Exponential run-time (worst case)

_**Write up the `rpl_1_2` example here**_


<!-- ----------------------------------------------------------------------------- -->
## Alternative approach

Why would we write a grammar like the one just presented?  Because it produces
ideal parse trees compared to the alternative.  Let's look at the typical
alternative, in which infix expressions are parsed linearly.

``` 
exp = { term { operator? exp}* }
term = { group / quoted_string / charset / identifier }
group = { "(" exp ")" }
```

The first line defines an expression, `exp`, as either a single `term` or a
series of the form `term operator term operator term ...`.

There is a grouping rule in the grammar above, called `group`.  A `term` in this
grammar can be a quoted string, a character set (like `[a-z]`), an identifier,
or a `group` expression, which is enclosed in parentheses, e.g `(x / [a-z])`.

When the grammar matches an infix expression like `a / b`, the match has type
`exp`.  Same when the input is simply `a`, or a more complex sequence of terms
and operators like `x / [a-z] & "cat" / y`.

This is unfortunate when compared to the grammar from the previous section.  For
the input `x / y`, the previous grammar returns a parse tree with a `choice` at
its root.  For the input `x / [a-z] & "cat"`, the resulting parse tree has
`choice` at its root; the first child is the identifier `x` and the second is an
`and` expression -- the `and` of `[a-z]` and `"cat"`.


_**Draw picture that compares parse trees here**_

In other words, the parse trees produced by the grammar from the previous
section are ideal for processing.  Walking the parse tree is as simple as
performing a post-order traversal of the parse tree, processing each subtree
before applying the operator at the root.

The grammar in this section, however, produces parse trees that are not so easy
to process.  When we see an `exp` parse tree node, we don't know what we will
find in the child nodes.  We may find simply a `term`.  Or we may find a
series, looking at the children from left to right, of a `term`, an
`operator`, and another `exp`.

Any algorithm that processes such a parse tree must take this into account,
looking down into the children of a node to determine which operator to apply at
that node.

Or, we could transform the "infix tree" produced by the grammar presented at the
start of this section into the "prefix tree" that would have been produced had
we used the grammar of the previous section.  We explore this option, which we
have implemented, in the next section.

<!-- ----------------------------------------------------------------------------- -->
## "Shunting yard" algorithm

Dijkstra's
[shunting yard algorithm](https://en.wikipedia.org/wiki/Shunting-yard_algorithm)
was designed exactly for this purpose.  Well, almost.  It processes a sequence
of terms interleaved with operators, like `x / [a-z] & "cat" / y`, such that the
operator appears last, i.e. in the post-order traversal that would have resulted
from processing a prefix tree version of the expression.  

In other words, the original _shunting yard_ algorithm would take the expression
`x / y` and produce a list of `x` and `y` followed by the operator `/`.  An
algorithm for processing such an expression would then apply the `/` operator to
the terms that came before, namely `x` and `y`.

When applied to an expression like `x / [a-z] & "cat" / y`, _shunting yard_
takes into account the relative precedence of the `/` and `&` operators, as well
as their default associativity (left or right), to produce exactly the correct
series of operations to be performed.

This post is not to explain _shunting yard_, but only to say that it is a linear
time algorithm for converting the output of the "infix grammar" of the previous
section into a more useful form.  The post-order form is not exactly what Rosie
needs, though.  Since Rosie produces a parse tree for later processing, we don't
want to combine the transformation of an "infix parse tree" with its processing.
We want to modify _shunting yard_ to produce a "prefix parse tree" just as if we
used the (exponential worst case) grammar from the previous section.

Fortunately, that modification is easy to do.  An implementation, which is
admittedly cluttered by real-world concerns, is in
[src/lua/infix.lua](https://gitlab.com/rosie-pattern-language/rosie/blob/dev/src/lua/infix.lua)
in the `dev` branch of the Rosie source repository.


_**Draw picture showing parse tree transformation by shunting yard algorithm**_


<!-- ----------------------------------------------------------------------------- -->
## Implementation

Each Rosie engine has its own compiler, which means that each engine can support
its own version of the RPL language.  The first phase of the RPL compiler is a
parser, and in Rosie, this parser itself uses an RPL grammar.  (Rosie uses Rosie
to parse Rosie, in a form of "self-hosting".)

The
[Rosie v1.2.0-preview.3](https://gitlab.com/rosie-pattern-language/rosie/-/tags/1.2.0-preview.3)
release has an "infix style" grammar that resembles the one at the start of the
[Alternative approach section](#alternative-approach).  The Rosie compiler
structure was modified to accept an optional post-processing function that runs
after parsing.  That allowed the replacement of
[rpl/rosie/rpl\_1\_2.rpl](https://gitlab.com/rosie-pattern-language/rosie/blob/1.2.0-preview.3/rpl/rosie/rpl_1_2.rpl)
as the RPL parser with
[rpl/rosie/rpl\_1\_3.rpl](https://gitlab.com/rosie-pattern-language/rosie/blob/1.2.0-preview.3/rpl/rosie/rpl_1_3.rpl)
with a _shunting yard_ post-processing step.

The result is the expected change in worst-case processing time, as shown in the
graph below.

<center>
<img width="80%" src="{{ site.baseurl }}/images/infix-prefix-graph.png" 
alt= "Contrast between exponential curve (rising quickly as a function of x) and
a straight line with very low slope, rising slowly as a function of x.  The
exponential curve is labeled as RPL 1.2 and the almost horizontal line is
labeled as RPL 1.3 with shunting yard.  The graph title explains that the x axis
is the number of parentheses surrounding a single identifier, where 3
corresponds to (((a))).  The y axis is processing time, so higher is worse.">
</center>


<!-- ----------------------------------------------------------------------------- -->
## Maybe a Rosie output encoder?

There are better ways to parse languages like RPL, and particularly any language
that is more complicated.  However, RPL is perfectly capable of parsing RPL.
The most straightforward RPL grammar for parsing RPL is _rpl\_1\_2_, but the way
it was written gives an exponential worst-case run time.  The alternative
"infix style" of the _rpl\_1\_3_ gives a linear worst-case run time, and when
followed by _shunting yard_, produces the same parse trees as _rpl\_1\_2_.  In
fact, the same Rosie compiler is used for both.

But that is the Rosie implementation.  What about RPL grammars written by Rosie
users?  Wouldn't they want the same capability, to define an "infix style"
grammar with linear worst-case parsing time, but use the (also linear time)
_shunting yard_ algorithm as a post-processor to transform the parse trees into
an ideal form?

We could enable this by providing the _shunting yard_ implementation that is now
part of Rosie in the form of a Rosie _output encoder_, which processes a Rosie
parse tree to generate useful output <small><a
name="cite-note-2">\[[Note 2](#note-2)\]</a></small>.  Today, an output encoder
starts with an internal representation and encodes it as JSON, or pretty-printed
JSON, or color text, or a simple boolean, etc.

One of the existing encoders could be combined with _shunting yard_ to produce,
for example, a JSON representation of a transformed parse tree.  And the
_shunting yard_ implementation we already have supports operator precedence and
default associativity in a configurable way, making it customizable.

But when a language is complex enough to contain several infix operators,
multiple grouping forms, and more, then a traditional approach to parsing, such
as using a parser generator, is likely the right solution.


<!-- ----------------------------------------------------------------------------- -->
## Conclusion

Like many discussions of algorithms and their uses, our conclusion has the
unsatisfying flavor of "it depends".  In other words, we have no blanket
recommendation or optimal solution.  For all practical purposes, a Parsing
Expression Grammar implementation, like Rosie/RPL, can handle any unambiguous
context-free grammar.  Programming languages and data description languages fall
into this group.

But is Rosie/RPL the right solution for parsing a programming language?
Probably not.  I have in mind this line of reasoning:

* To avoid exponential worst-case performance, the grammar must be carefully
  designed.  Specifically, the combination of grouping syntax and infix
  expressions can produce an undesirable situation.
* The knowledge of how to design a recursive descent grammar, like a PEG, for
  good worst-case performance is not particularly deep or difficult, but it is
  unfamiliar to many (maybe most) programmers.  It is specialized knowledge.
* Generally, Rosie/RPL makes it difficult to accidentally write bad-performing
  patterns.  But the RPL `grammar` feature enables recursion, which opens the
  door to bad worst-case performance when a grammar is written in certain
  problematic ways.
* Still, a developer who needs a parser for a complex language (like RPL itself,
  or something even more intricate) could use Rosie/RPL.
    * That's what I did.  And I considered instead living with the exponential
      run-time needed to parse expressions like `((((a))))`, because they are
      rare in practice.  Ultimately, I chose to change the RPL grammar and
      implement _shunting yard_ to get back the parse trees I had and liked
      before.
    * What should a Rosie user do in a similar situation?  They could write an
      "infix style" grammar like the one in the
      [Alternative approach section](#alternative-approach) to get linear-time
      parsing, and implement _shunting yard_ as a post-processor.  Or they could
      work with the Rosie Project to make _shunting yard_ available as a
      customizable Rosie output encoder.
	* Or, the Rosie user could change tools, and use a parser generator
      instead.  Using a parser generator requires some specialized knowledge
      about parsing algorithms and grammar design.  But that knowledge, while
      niche, is not out of reach for a developer.
    * Another important consideration is whether the overall goals of the
      developer are best met with a parser generator that allows custom actions
      to execute during parsing.  Rosie itself demonstrates that this is not
      always needed: Rosie does a complete parse of RPL statements and
      expressions, and the resulting parse tree is later subject to macro
      expansion and other transformation before being compiled.


In a [related post]({{ site.baseurl }}{% post_url
2019-06-10-To-Parse-or-Not-to-Parse %}), I talked about whether Rosie is a
pattern matcher with close kinship to regex or a parser that happens to be good
at pattern matching.  The name _Rosie Pattern Language_ hints at my view:
Rosie/RPL is a good pattern matcher that can also be deployed to parse a variety
of languages.  One of the ways that RPL is an improvement over regex is that RPL
is based on a well-understood formal model (Parsing Expression Grammars),
whereas regex accrued new features, like recursion, in an ad hoc way that varies
greatly across implementations.  (And regex recursion is subject to the same
worst-case limitations.  In fact, you don't even need recursion to make regex
perform [exponentially badly](https://swtch.com/~rsc/regexp/regexp1.html).)

So, yes, Rosie is a parser.  But no, it's not the best parser for every situation.  I
would argue, though, that Rosie is a really good pattern matcher that was
designed to capably match (parse) recursively defined input (e.g. JSON, HTML,
small programming languages) as well as everything that regex can match.

---


<span style="color:darkred;text-align:center">
_Follow Dr. Jamie Jennings on [Twitter](https://twitter.com/jamietheriveter) for
Rosie announcements and other miscellanea._
</span>

---
#### Note 1

The differences are:

* Regex use an unordered choice operator, denoted by the pipe symbol, `|`,
  whereas RPL and other
  [Parsing Expression Grammars](https://en.wikipedia.org/wiki/Parsing_expression_grammar)
  use an ordered choice operator, denoted by a slash, `\`.

* The RPL (and PEG) repetition operators are possessive, meaning that once they
  match, they are never re-considered.

[Return](#cite-note-1)


---
#### Note 2

Most Rosie output encoders are not parameterized.  The `json` and `jsonpp`
encoders, for example, are not customizable.  But the `color` output encoder
takes a parameter which is a string assigning colors to pattern names.  That
string can be supplied on the command line, in a `~/.rosierc` file, or via the
`librosie` API.  So there is precedent for an output encoder that, like
_shunting yard_, uses parameters for customization.

Some design work would have to be done to determine how best to supply a
_shunting yard_ output encoder with information about the infix operators, their
precedence, and associativity.  In addition, some aspects of the RPL grammar
made the interface to _shunting yard_ more complicated than it needs to be.
Rather than make more changes to the existing _rpl\_1\_2_ grammar, I chose to
write a case-based interface to _shunting yard_.  It remains to be demonstrated
that a very simple interface (API) would suffice.  It's possible that some
additional parameters would need to be supplied by the user to make this
practical. 

[Return](#cite-note-2)


