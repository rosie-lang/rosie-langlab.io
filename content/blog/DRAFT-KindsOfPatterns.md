---
draft: true
title:  "Kinds of Patterns"
description: "DRAFT"
lastmod: "2019-10-11"
---

Textual pattern matching encompasses several use cases, and they need distinct
kinds of patterns.  Input validation requires strict patterns that exactly the
valid inputs.  Data mining often requires "fuzzy" patterns that match input
which is in approximately the right format (e.g. due to errors in the data).
Further, there are interesting variations on both of these primary use cases,
and each variation suggests a different approach to pattern writing.

First, let's note that syntax-based pattern matching that is done by regex or
Rosie/RPL or another text matching tool is rarely the last step in handling a
piece of data.  The point of matching against a pattern is to recognize a
sequence of characters that is useful for subsequent processing.  For example,
if I extract an email address from a web page, I will almost certainly use that
email address at some future time, either to send an email or to compare to a
stored string (like a user id).

Often we can think of these later uses as relying on the _semantics_ of the data
that was identified initially, during pattern matching, by its _syntax_.


(1) Can use syntax to identify usable values; by definition, a usable value can
be used directly, with only transient errors.  E.g. an email address may not be
deliverable at this moment, but can still be a valid email syntactically.
E.G. INPUT VALIDATION
EXACT PATTERNS i.e. no further syntactic analysis is needed

(2) Too expensive to identify usable values by syntax; some unusable values will
match; perhaps we are trading off the cost of syntactic validation for the cost
of post-processing (e.g. semantic validation).
E.G. JSON-LIKE OBJECT
PERMISSIVE PATTERNS i.e. more syntactic analysis is needed

(3) Input may have errors; some directly unusable values will match, but there
exists a (perhaps fallible) conversion function that turns such values into
usable values.
E.G. "info [AT] rosie-lang [DOT] org"
EXACT PATTERNS but with deterministic post-processing
E.G. "Ibm" means "IBM Corporation of Armonk, NY"
EXACT PATTERNS but with non-deterministic post-processing (e.g. entity
resolution, where the same input tomorrow may give a different answer because
the ER database changed)

(A) For whatever reason, we decide to match only a subset of the "proper type"
of object.  E.g. "https:" is a valid URI, as is "https:a.b.c".  The "authority"
field is missing in both examples, and the "path" is missing in the first but
has the value "a.b.c" in the second.  So rather than match all valid URIs, we
may choose to match only the formats commonly in use, e.g. "https://rosie-lang.org".
E.G. `net.url_strict` (which matches all 3) versus `net.url` (matches only the rosie one)
RESTRICTIVE PATTERNS i.e. will reject syntactically valid values
Note: These are only useful if it matches a proper subset of the exact version
of the pattern. 

CAN COMBINE (A) WITH (1), (2), or (3).

(1A) "Enter an ncsu email address."  The matched value must also match the exact
pattern. 
(2A) Expecting a JSON Array, and only willing to match braces with pattern.  The
matched value must also match the "approximate JSON" pattern, but may not match
an exact pattern.
(3A) Web scraping for ncsu emails; or do ER only on title case words.  The
matched value must also match the exact pattern for the "data with errors".

---





Data mining: Input may have errors.




'Syntactic match' (possibly) produces a 'usable value' for testing semantic
validity. 

MUST ADDRESS "TYPE" (or "type tag"): DO WE KNOW WHAT IT IS BEFORE WE MATCH IT
AGAINST A PATTERN?

Input validation:
  We know the "type", e.g. email address or postal code.
  Want a "usable value", typically, because semantic validity is not important
  or not knowable (e.g. an email address may be valid now but not a few seconds
  from now).
  Want the value to have the identified "type".  (This is like dependent types,
  right?  We have a value-dependent type checker.)
  Want: syntactic match --> usable value
  Sub-cases:
  - Don't care about the converse. E.g. there may be usable values that do not
    match the pattern.  The pattern is "restrictive" with respect to the type;
    it will fail to match values of the type (e.g. only matching the values we
    "care about"). 
  - Want the converse as well, i.e. syntactic match <-- usable value, i.e. the
    pattern should match exactly the set of values in the type.  The pattern is
    "exact".

Data mining (searching through data for info):

  Don't have certainty about the semantics of whatever part of the data we are
  matching against, e.g. "follow@ibm" in a document could be an email address
  (at the relative domain "ibm") or a suggestion to follow a certain twitter
  account. 

  We want matches that could be values of whatever type we are searching for.
  These matches (e.g. possible email addresses) should be syntactically valid
  input for whatever processing will follow.
  
  Want a "usable value" for further processing, e.g. semantic validity testing
  ("Is this a valid U.S. postal code?") or entity resolution ("Is this the name
  of a person in the company?").

  In this sense, what we want is syntactic match --> usable value.  The fact
  that a match may also be a usable value of another type is an orthogonal
  issue, though one of concern to the larger data mining endeavor.

  Sub-cases:
  - We also want the converse, syntactic match <-- usable value.  That is, we
    want the pattern to exactly match the set of possible values.

  - Don't care about the converse.  We are willing to match values not of the
    identified type.  Some possible reasons:
	- An exact syntactic match can be expensive, e.g. matching JSON is recursive
      and could subject the matcher to a denial of service attack if an attacker
      can supply a large adversarial input.  Or, instead of being recursive, an
      exact pattern would be overly long, e.g. the set of valid (in use)
      U.S. zip codes.  In these cases our pattern "screens" for possible
      matches.
    - We don't have confidence in the actual type of the input data, so we know
      that 
	

  Case 1: Want syntactic match <-- usable value.
  Converse is characteristically not achievable because the use case is one in
  which the actual "type" of the data (semantics) is not known.
  The pattern is designed to be "permissive" of necessity.

  Note: A usable email address could be "info [AT] rosie [DOT] org" in one
  system (one that can convert to "info@rosie.org"), but not in another (e.g. an
  interface to SMTP).  
  
  The pattern is "permissive" with respect to the type; it will match things
  that are not of the desired type.

  Case 2: Want syntactic match --> usable value.
  Here, a "usable value" is anything worth processing further for validity.  We
  have eliminated values that we know cannot be valid.
  E.g.: A string starting with an opening bracket
  `[` or brace `{`, ending with its opposite <!-- }] --> and having balanced
  brackets and braces in between, COULD be JSON-encoded data, but might not be.
  A pattern like this could "screen for JSON objects/arrays", making a quick
  calculation instead of a full parse.  Such a pattern is "restrictive
  from a JSON viewpoint, because it can match only objects and arrays (not, say,
  strings) but that is not the right perspective.  The pattern is "permissive"
  because it can match invalid JSON objects like "{0,1}".

  Note: An unusable domain name could be "a".  While unqualified and partially
  qualified domain names are valid in general, they may not be usable in a data
  mining context.  In the case of this example, "a" could be a relative domain
  name, but without context suggesting that it is (i.e. more certainty about its
  "type"), we should hesitate before declaring "a" a domain name.  A pattern
  like `net.fqdn` (footnote: we know it is badly named because it matches
  relative domain names as well) matches "a", whereas `net.fqdn_practical` does
  not.  `fqdn_practical` was written specifically to avoid matching ordinary
  words, i.e. specifically for this use case, where we want every syntactic
  match to be a usable value.  `fqdn_practical` was designed to reject values
  that are usable (e.g. could be a valid domain name).  This design decision was
  made with external knowledge of the use case, which might be stated "in a
  document of English text, ordinary words are very unlikely to be domain
  names."  Like the JSON example, it is an error to say that `fqdn_practical` is
  restrictive here.  Once external forces have constrained our approach (e.g. to
  ignore ordinary words), we recognize that the pattern itself is permissive --
  it allows
  
<!--   With this -->
<!--   pattern, a usable value (like "a" for a domain name) will not match. -->

<!--   pattern like `net.url` (which matches only a common subset of URLs) appear -->
<!--   "restrictive".  But like the JSON example, this is the wrong perspective.  The -->
<!--   pattern `net.url` is "permissive" in that it can match invalid URLs like -->
<!--   "oh://wow.tay", with non-existent scheme names ("oh") and top level domains -->
<!--   ("tay").  -->


Permissive:
  syntactic match <--> usable value but NOT converse
  some matches will not be semantically valid
  e.g. not every 5-digit number is a zip code
  
Restrictive:
  syntactic match --> usable value
  some usable values will not be matched
  e.g. "a" is a valid domain name (partially qualified), as is every other
  single ASCII letter. but typical URL patterns will not accept a single letter
  for a domain name.

Exact:
  Neither permissive nor restrictive
  syntactic match <--> semantically valid

semantically valid --> syntactic match
  - where a match to the pattern is NECESSARY (but not sufficient)
  - not sufficient: "Colorless green ideas sleep furiously" is valid
    syntactically but not semantically.

syntactic match --> usable (resolvable)
  - where a syntactic match is SUFFICIENT (but not necessary) for USE
  - usage can fail, of course, but not for invalid syntax
  - not necessary: "info [AT] rosie [dot] org" is not a valid email address but
    we can algorithmically convert it to one we can use.

Input validation
- Syntactically valid ALWAYS; this is a NECESSARY and SUFFICIENT CONDITION for
  semantic validation, and a NECESSARY CONDITION for USAGE of the value.
- Semantically? 
  - May well be time-dependent, e.g. valid email right now does not imply it
    will be valid tomorrow.
  - Typically, no semantic validation is done before the value is used.  An
    example usage might be sending a "confirm that this is your email address"
    message. 

Data mining
- Syntactically valid
  - Semantically, may be time-dependent as noted above; or
  - May not be semantically valid at all, e.g. domain name with an invalid
    top-level domain.  In that example, an identifier like "com.microsoft.tay"
    could be mistaken for a domain name, though "tay" is not a top level domain
    as of this writing.  Consider also "com.ibm.java", which is most likely an
    identifier, although "java" is in fact a top level domain name as of 2015.
- Syntactically invalid
  - But algorithmically convertible to a valid form, e.g. 'info [at] rosie [dot]
    org', after which semantic validity can be tested
  - Where the use case involves Entity Resolution, a process that maps an input
    to most likely entity it represents, e.g. "ibmcorp" to IBM Corporation of Armonk,
    NY, U.S.A.; or "New Jersy" to the U.S. state of New Jersey.




For input validation, such as validating an email address,
the pattern must match only valid (usable) email addresses.  For data mining,
such as extracting postal codes, we probably want a pattern that matches text
that is _likely_ to be a postal code, since other data might share the same
formatting (e.g. U.S. zip codes are commonly written as 5 digits, so any 5-digit
number could be a zip code).



--- 
**We welcome feedback and contributions!**
* Please open issues (or merge requests) on
  * the main [GitLab](https://gitlab.com/rosie-pattern-language/rosie)
    repository; or
  * the community [GitLab](https://gitlab.com/rosie-community) repository.
* Join our mailing list for general questions and discussion: [https://groups.io/g/rosiepattern](https://groups.io/g/rosiepattern). 
* Get in touch [by email](mailto:info@rosie-lang.org). 
* Follow the Rosie project on [Twitter](https://twitter.com/rosiepattern).
* Follow Dr. Jamie Jennings on [Twitter](https://twitter.com/jamietheriveter) for
computer science, software development, and randomness.
--- 
